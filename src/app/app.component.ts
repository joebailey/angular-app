import { Component } from '@angular/core';
import 'skyslope-toggle-option-group/skyslope-toggle-option-group.js';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'angular-app';
  options = ["Yes", "No", "Maybe"];
  onChange(e: Event): void {
    console.log ('Inside onChange...', e);
    this.selectedValue = e.detail.value;
  };
  selectedValue = '';
}
